//
//  Object.swift
//  p03-langert
//
//  Created by Erik Langert on 2/19/17.
//  Copyright © 2017 Erik Langert. All rights reserved.
//

import Foundation
import UIKit

class Object : UIView {
    var velocity = (0.0, 0.0)
    var acceleration = (0.0, 0.0)
    var location = (0.0, 0.0)
    func draw() {
        let context = UIGraphicsGetCurrentContext()
        CGContextSaveGState(context)
        CGContextSetLineWidth(context, 4.0)
        let rectangle = CGRect(x: location.0, y: location.1, width: 100, height: 100)
        let clipPath: CGPathRef = UIBezierPath(roundedRect: rectangle, byRoundingCorners: [UIRectCorner.TopLeft, UIRectCorner.BottomLeft], cornerRadii: CGSize(width: 3.0, height: 6.0)).CGPath
        super.drawRect(rectangle)
        CGContextAddPath(context, clipPath)
        CGContextSetFillColorWithColor(context, UIColor.redColor().CGColor)
        CGContextClosePath(context)
        CGContextFillPath(context)
        CGContextRestoreGState(context) /* context?.addRect(context, rectangle) CGStrokePath(context ) */ } }