//
//  ViewController.swift
//  p03-langert
//
//  Created by Erik Langert on 2/19/17.
//  Copyright © 2017 Erik Langert. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollisionBehaviorDelegate {
    
    var animator: UIDynamicAnimator!
    var player_physics: UIDynamicItemBehavior!
    var gravity: UIGravityBehavior!
    var collision: UICollisionBehavior!
    var square: UIView!
    var barrier: UIView!
    var snap: UISnapBehavior!

    override func viewDidLoad() {
        
        let poodle = UIImage(named: "poodle.png")
        square = UIImageView(image: poodle)
        
        square.frame = CGRect(x:100, y:100, width:100, height:100)
        //square = UIView(frame: CGRect(x: 100, y: 100, width: 100, height: 100))
        square.backgroundColor = UIColor.grayColor()
        view.addSubview(square)
        print("TESTING")
        
        let bone = UIImage(named: "bone.png")
        barrier = UIImageView(image: bone)
        barrier.frame = CGRect(x: 20, y: 300, width: 100, height:50)
        //barrier = UIView(frame: CGRect(x: 20, y: 300, width: 100, height: 35))
        //barrier.backgroundColor = UIColor.whiteColor()
        view.addSubview(barrier)
        
        
        animator = UIDynamicAnimator(referenceView: view)
        player_physics = UIDynamicItemBehavior(items: [square])

        gravity = UIGravityBehavior(items: [square])
        animator.addBehavior(gravity)
        
        collision = UICollisionBehavior(items: [square])
        collision.collisionDelegate = self
        collision.addBoundaryWithIdentifier("barrier", forPath: UIBezierPath(rect: barrier.frame))
        collision.translatesReferenceBoundsIntoBoundary = true
        animator.addBehavior(collision)
        let itemBehavior = UIDynamicBehavior()
        animator.addBehavior(itemBehavior)
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    /*
    func collisionBehavior(_ behavior: UICollisionBehavior,
        beganContactFor item1: UIDynamicItem,
                   with item2: UIDynamicItem,
                     at point : CGPoint) {
            print("HALP")
            print("Boundary contact occurred - \(item2)")
    }
*/
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if let touch = touches.first {
            let screenSize: CGRect = UIScreen.mainScreen().bounds
            let position: CGPoint = touch.locationInView(view)
            
            let bone = UIImage(named: "bone.png")
            barrier = UIImageView(image: bone)
            barrier.frame = CGRect(x: Int(arc4random()) % Int(position.x), y: Int(arc4random()) % Int(position.y), width: 100, height:50)
            
            let touchDistance = (screenSize.width / 2.0) - position.x
            player_physics.addLinearVelocity(CGPoint(x: touchDistance, y: 0), forItem:square )
        }
    }

    
    func collisionBehavior(behavior: UICollisionBehavior!, beganContactForItem item: UIDynamicItem!, withBoundaryIdentifier identifier: NSCopying!, atPoint p: CGPoint) {
        print("Boundary contact occurred - \(identifier)")
    }

    /*
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if (snap != nil) {
            animator.removeBehavior(snap)
        }
        
        let touch = touches.first as UITouch!
        snap = UISnapBehavior(item: square, snapToPoint:  touch.locationInView(view))
        animator.addBehavior(snap)
    }
*/

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

